package whiteboard.cloning;

/**
 * Created by jlockrid on 12/2/2016.
 */
public class Soul {

    private final String soulDestination;
    public Soul(String soulDestination){

        this.soulDestination = soulDestination;
    }

    public String getSoulDestination(){

        return soulDestination;
    }
}
