package whiteboard.cloning;

/**
 * Created by jlockrid on 12/2/2016.
 */
public class Person extends Human implements Cloneable{

    private final String name;
    public Person(String name, int age, String sex, Soul soul) {
        super(age, sex, soul);
        this.name = name;
    }


    public String getName() {
        return name;
    }


    @Override
    public Person clone() throws CloneNotSupportedException {
        return (Person)super.clone();
    }
}
