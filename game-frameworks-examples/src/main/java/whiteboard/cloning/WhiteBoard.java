package whiteboard.cloning;

/**
 * Created by jlockrid on 12/2/2016.
 */
public class WhiteBoard {



    public static void main(String args[]){
        Soul hell = new Soul("hell");
        Soul heaven = new Soul("heaven");
        Human h = new Human(25, "Male", hell);

        System.out.println("human="+h+" age="+h.getAge()+" human sex="+h.getSex()+" soul="+
                h.getSoul()+" soul dest="+h.getSoul().getSoulDestination());

//        try {
//            Human anotherH = (Human)h.clone();
//            System.out.println("ahuman="+anotherH+" age="+h.getAge()+" ahuman sex="+anotherH.getSex()+" soul="+
//                    anotherH.getSoul()+" soul dest="+anotherH.getSoul().getSoulDestination());
//        }catch(Exception e){e.printStackTrace();}



        Person jova = new Person("chuck", 27, "male", hell);
        System.out.println("jova="+jova+" name= "+jova.getName()+" age="+jova.getAge()+" jova sex="+jova.getSex()+" soul="+
                jova.getSoul()+" soul dest="+jova.getSoul().getSoulDestination());


        try{

            Object o = jova.clone();
            System.out.println("o.getClass(): "+o.getClass());
            Person jova1=  (Person) o;
            System.out.println("jova="+jova1+" name= "+jova1.getName()+" age="+jova1.getAge()+" jova sex="+jova1.getSex()+" soul="+
                    jova1.getSoul()+" soul dest="+jova1.getSoul().getSoulDestination());




        }catch(Exception e){e.printStackTrace();}

    }
}
