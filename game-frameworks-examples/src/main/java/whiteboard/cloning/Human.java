package whiteboard.cloning;

/**
 * Created by jlockrid on 12/2/2016.
 */
public class Human  {
    private int age;
    private String sex;
    private final Soul soul;


    public Human(int age, String sex, Soul soul){
        this.age = age;
        this.sex = sex;
        this.soul = soul;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public Soul getSoul() {
        return soul;
    }

//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        return super.clone();
//    }
}
