package examples.gui;

/**
 * Created by jlockrid on 10/3/2016.
 */
public class Student extends Person {

    public int getAge() {
        return age;
    }

    private int age = 20;
    void tell() {
        System.out.println("call-Student");
    }
}
