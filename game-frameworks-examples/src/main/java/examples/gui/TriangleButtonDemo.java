package examples.gui;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.Random;

/**
 * Created by jlockrid on 9/28/2016.
 */
public class TriangleButtonDemo extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        TriangleButton button = new TriangleButton();
        button.setId("my_trinagle_button");
        button.setPadding(new Insets(20));
        button.setOnAction((e) -> {
            Random random = new Random(System.currentTimeMillis());
            button.setBackgroundFill(Color.color(random.nextDouble(), random.nextDouble(), random.nextDouble()));
            System.out.println("Target "+e.getTarget()+" Source "+e.getSource());
        });

        StackPane myPane = new StackPane();
        myPane.getChildren().add(button);

        Scene myScene = new Scene(myPane);
        primaryStage.setScene(myScene);
        primaryStage.setWidth(300);
        primaryStage.setHeight(200);
        primaryStage.show();

//        Button b = new Button();
//        b.setBackground();

    }


    public static void main(String []args){
        launch(args);
    }
}
