package examples.gui;

import com.guigarage.css.CssHelper;
import com.guigarage.css.DefaultPropertyBasedCssMetaData;
import javafx.css.converter.PaintConverter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.css.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

import java.util.List;

public class TriangleButton extends Control {
    private StyleableObjectProperty<Paint> backgroundFill;

    public TriangleButton(){
        getStyleClass().add("triangle");
    }

    private ObjectProperty<EventHandler<ActionEvent>> onAction =
            new ObjectPropertyBase<EventHandler<ActionEvent>>() {

        @Override
        protected void invalidated(){
            setEventHandler(ActionEvent.ACTION, get());
        }

        @Override
        public Object getBean() {
            return TriangleButton.this;
        }

        @Override
        public String getName() {
            return "onAction";
        }
    };


    @Override
    protected Skin<?> createDefaultSkin(){
        return new TriangleButtonSkin(this);
    }

    public Paint getBackgroundFill(){
        return backgroundFill == null ? Color.DARKGRAY : backgroundFill.get();
    }

    public void setBackgroundFill(Color backgroundFill){
        this.backgroundFill.set(backgroundFill);
    }

    public StyleableObjectProperty<Paint> backgroundFillProperty(){
        if(backgroundFill == null){
            backgroundFill = CssHelper.createProperty(StyleableProperties.BACKGROUND_FILL, TriangleButton.this);
        }

        return backgroundFill;
    }

    public final ObjectProperty<EventHandler<ActionEvent>> onActionProperty() {
        return onAction;
    }



    public final void setOnAction(EventHandler<ActionEvent> value){
        onActionProperty().set(value);
    }



    private static class StyleableProperties{

        private static final DefaultPropertyBasedCssMetaData<TriangleButton, Paint> BACKGROUND_FILL =
                CssHelper.createMetaData("-fx-triangle-fill", PaintConverter.getInstance(), "backgroundFill", Color.LIGHTGREEN);

        private static final DefaultPropertyBasedCssMetaData<TriangleButton, Paint> STROKE_FILL =
                CssHelper.createMetaData("-fx-triangle-stroke", PaintConverter.getInstance(), "strokeFill", Color.BLUE);

        private static final List<CssMetaData<? extends Styleable, ?>> STYLEABLES =
                CssHelper.createCssMetaDataList(Control.getClassCssMetaData(), BACKGROUND_FILL, STROKE_FILL);

       }

    @Override
    public List<CssMetaData<? extends Styleable, ?>> getControlCssMetaData() {
        return getClassCssMetaData();
    }

    public static List<CssMetaData<? extends Styleable, ?>> getClassCssMetaData(){
        return StyleableProperties.STYLEABLES;
    }


}
