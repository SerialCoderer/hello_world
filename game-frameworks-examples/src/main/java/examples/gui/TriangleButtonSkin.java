package examples.gui;

import examples.gui.TriangleButton;
import javafx.beans.InvalidationListener;
import javafx.css.StyleableProperty;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.SkinBase;
import javafx.scene.paint.Color;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;

/**
 * Created by jlockrid on 9/28/2016.
 */
public class TriangleButtonSkin extends SkinBase<TriangleButton> {

    private Path triangle;
    private boolean invalidTriangle = true;

    private InvalidationListener updateTriangleColorListener = observable -> updateTriangleColor();

    protected TriangleButtonSkin(TriangleButton control){
        super(control);
        control.backgroundFillProperty().addListener(updateTriangleColorListener);
    }


    public void updateTriangleColor(){
        if(triangle != null){
            triangle.setFill(getSkinnable().getBackgroundFill());
            getSkinnable().requestLayout();
        }
    }
    public void updateTriangle(double width, double height){

        if(triangle != null ){
            getChildren().remove(triangle);
        }
        triangle = new Path();
        triangle.getElements().add(new MoveTo(width / 2, 0));
        triangle.getElements().add(new LineTo(width, height));
        triangle.getElements().add(new LineTo(0, height));
        triangle.getElements().addAll(new ClosePath());
        triangle.setStroke(Color.BLACK);
        triangle.setFill(getSkinnable().getBackgroundFill());

        triangle.setOnMouseClicked((e) -> getSkinnable().fireEvent(new ActionEvent()));
        getChildren().add(triangle);
    }

    @Override
    protected void layoutChildren(double contentX, double contentY, double contentWidth, double contentHeight){
        if(invalidTriangle){
            updateTriangle(contentWidth, contentHeight);
            invalidTriangle = false;
        }
        layoutInArea(triangle, contentX, contentY, contentWidth, contentHeight, -1, HPos.CENTER, VPos.CENTER);
    }

    @Override
    protected double computePrefHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset){
        return topInset + bottomInset + 200;
    }

    @Override
    protected double computePrefWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset){
        return rightInset + leftInset +200;
    }

    @Override
    protected double computeMinHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset){
        return 20 + topInset + bottomInset;
    }

    @Override
    protected double computeMinWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset){
        return 20 + rightInset + leftInset;
    }

    @Override
    protected double computeMaxWidth(double height, double topInset, double rightInset, double bottomInset, double leftInset){
        return computePrefWidth(height, topInset, rightInset, bottomInset, leftInset);
    }

    @Override
    protected double computeMaxHeight(double width, double topInset, double rightInset, double bottomInset, double leftInset){
        return computePrefHeight(width, topInset, rightInset, bottomInset, leftInset);
    }




}
